#include <Arduino.h>
#include <SPI.h>
#include <MFRC522.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

#define SS_PIN D8
#define RST_PIN D3

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key; 

const char* ssid = "teste";
const char* password = "senior123";
const char* mqtt_server = "broker.mqtt-dashboard.com";
String serverName = "http://192.168.137.33:8080/api/client/package/";
String hexCode = "";

String criciuma = "92cbb409-5a27-443e-b94d-dcd70c011f530";
String tubarao = "475cd197-6b8b-4335-b5f9-b14a33b34d65";
String itajai = "8e3c0afe-29fe-4f1c-beea-b991369a2d5e";
String floripa = "d5b91098-a30f-45eb-b4d3-e70dc683ccf1";
String loggedUnity = criciuma;

int trig = D1;
int echo = D2;

boolean ledStatus = false;

int interval;
float distance;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  
  pinMode(trig, OUTPUT);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  SPI.begin();
  rfid.PCD_Init(); 

  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
} 
  
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  if (readDistance() < 10){
    ledStatus = readRFID();
    if (ledStatus){
      client.publish("publicacao_esteira", "1");
      delay(2000);
    }else{
      client.publish("publicacao_esteira", "0");
    }
  }
}

float readDistance(){
  
  int interval;
  float distance;

  digitalWrite(trig, LOW);
  delayMicroseconds(10);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  interval = pulseIn(echo, HIGH);
  distance = interval*0.034/2;
  
  return distance;
}

void printHex(byte *buffer, byte bufferSize) {
  hexCode = "";
  for (byte i = 0; i < bufferSize; i++) {
    hexCode += hexCode.length() > 0 ? buffer[i] < 0x10 ? " 0" : " " : "";
    hexCode += String(buffer[i], HEX);
  }
  hexCode.toUpperCase();

  client.publish("publicacao_rfid", hexCode.c_str());
  if(findLog()){
    updateLog("{\"rfid\":\""+hexCode+"\",\"unityId\":\""+loggedUnity+"\"}");
  } else {
    createLog("{\"rfid\":\""+hexCode+"\",\"unityId\":\""+loggedUnity+"\"}");
  }
  
 
}

boolean findLog(){
  DynamicJsonDocument doc(1024);
  WiFiClient wifiClient;
  HTTPClient http;
  boolean hasPackage = false;
  String urlString = urlEncode(hexCode);
  http.begin(wifiClient, serverName+"find?rfid="+urlString);
  
  int httpResponseCode = http.GET();
 
  String payload = http.getString();
  deserializeJson(doc, payload);

  if (httpResponseCode > 0) {
     hasPackage = doc["hasPackage"];
  } else {
    client.publish("publicacao_backend", doc["ServerMessage"]["message"]);
  }
        
  http.end();
  return hasPackage;
}

void createLog(String data){
  DynamicJsonDocument doc(1024);
  WiFiClient wifiClient;
  HTTPClient http;
  http.begin(wifiClient, serverName+"create?");
  http.addHeader("Content-Type", "application/json");

  int httpResponseCode = http.POST(data);

  String payload = http.getString();
  deserializeJson(doc, payload);

  if (httpResponseCode > 0) {
    client.publish("publicacao_backend", doc["ServerMessage"]["message"]);
  } else {
    client.publish("publicacao_backend", doc["ServerMessage"]["message"]);
  }
        
  http.end();
}

void updateLog(String data ){
  DynamicJsonDocument doc(1024);
  WiFiClient wifiClient;
  HTTPClient http;
  http.begin(wifiClient, serverName+"track?");
  http.addHeader("Content-Type", "application/json");

  int httpResponseCode = http.PUT(data);
  String payload = http.getString();
  deserializeJson(doc, payload);

 if (httpResponseCode > 0) {
    client.publish("publicacao_backend", doc["ServerMessage"]["message"]);
  } else {
    client.publish("publicacao_backend", doc["ServerMessage"]["message"]);
  }
        
  http.end();
}

boolean readRFID(){

  if ( ! rfid.PICC_IsNewCardPresent())
    return false;

  if ( ! rfid.PICC_ReadCardSerial())
    return false;

  printHex(rfid.uid.uidByte, rfid.uid.size);
   
  rfid.PICC_HaltA();

  rfid.PCD_StopCrypto1();

  return true;
  
}

void setup_wifi() {

  delay(10);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(String topic, byte* payload, unsigned int length) {
  String msg = "";
  for (int i = 0; i < length; i++) {
    msg += (char)payload[i];
  }

 if(topic == "publicacao_unidade"){
   Serial.print("De: ");
   Serial.println(loggedUnity);
   if (msg == "criciuma"){
     loggedUnity = criciuma;
   }else if(msg == "tubarao"){
     loggedUnity = tubarao;
   }else if(msg == "itajai"){
     loggedUnity = itajai;
   }else if(msg == "floripa"){
     loggedUnity = floripa;
   }else{
     loggedUnity = criciuma;
   }
   
   Serial.print("Para: ");
   Serial.print(loggedUnity);
   Serial.print(" - ");
   Serial.println(msg);
 }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.println("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.subscribe("publicacao_unidade");
     } else {
      Serial.println("failed, rc=");
      Serial.println(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

String urlEncode(String str)
{
    String encodedString="";
    char c;
    char code0;
    char code1;
    char code2;
    for (int i =0; i < str.length(); i++){
      c=str.charAt(i);
      if (c == ' '){
        encodedString+= '+';
      } else if (isalnum(c)){
        encodedString+=c;
      } else{
        code1=(c & 0xf)+'0';
        if ((c & 0xf) >9){
            code1=(c & 0xf) - 10 + 'A';
        }
        c=(c>>4)&0xf;
        code0=c+'0';
        if (c > 9){
            code0=c - 10 + 'A';
        }
        code2='\0';
        encodedString+='%';
        encodedString+=code0;
        encodedString+=code1;
      }
      yield();
    }
    return encodedString;
    
}